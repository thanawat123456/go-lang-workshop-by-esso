package main

import "fmt"

type Student struct {
	Name  string
	Age   string
	Score string
}

func main() {
	s := Student{
		Name:  "John Doe",
		Age:   "20",
		Score: "80.5",
	}
	fmt.Println("Name:", s.Name)
	fmt.Println("Age:", s.Age)
	fmt.Println("Score:", s.Score)
}
