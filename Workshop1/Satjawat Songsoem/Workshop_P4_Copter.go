package main

import "fmt"

var name string

func main() {

	fmt.Printf("Enter your name :")
	fmt.Scanf("%s", &name)
	fmt.Println("Hello,", name)
}
