package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var Db *sql.DB

const CustomerPath = "Customer"
const basePath = "/api"

type Customer struct {
	CustomerID   int    `json:"Customer_id"`
	CustomerName string `json:"Customer_name"`
	PhoneNumber  string `json:"Phone_number"`
	DateCreated  string `json:"Date_created"`
}

func SetupDB() {
	var err error
	Db, err = sql.Open("mysql", "root:Copter15!@tcp(127.0.0.1:3306)/customersdb")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(Db)
	Db.SetConnMaxIdleTime(time.Minute * 3)
	Db.SetMaxOpenConns(10)
	Db.SetMaxIdleConns(10)
}

func getCustomerList() ([]Customer, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	results, err := Db.QueryContext(ctx, `SELECT
	Customer_id,
	Customer_name,
	Phone_number,
	Date_created
    FROM Customers`)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	defer results.Close()
	customers := make([]Customer, 0)
	for results.Next() {
		var customer Customer
		err := results.Scan(&customer.CustomerID,
			&customer.CustomerName,
			&customer.PhoneNumber,
			&customer.DateCreated,
		)
		if err != nil {
			log.Println(err.Error())
			return nil, err
		}

		customers = append(customers, customer)
	}
	return customers, nil
}

func SetupRoutes(apiBasePath string) {
	customerHandler := http.HandlerFunc(handleCustomer)
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, CustomerPath), corsMiddleware(customerHandler))

	customersHandler := http.HandlerFunc(handleCustomers)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, CustomerPath), corsMiddleware(customersHandler))

}

func handleCustomer(w http.ResponseWriter, r *http.Request) {
	urlPathSegment := strings.Split(r.URL.Path, fmt.Sprintf("%s/", CustomerPath))
	if len(urlPathSegment[1:]) > 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	customerID, err := strconv.Atoi(urlPathSegment[len(urlPathSegment)-1])
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	switch r.Method {
	case http.MethodGet:
		customer, err := getCustomers(customerID)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if customer == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		j, err := json.Marshal(customer)

		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}

	case http.MethodDelete:
		err := removeCustomer(customerID)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

func getCustomers(customerid int) (*Customer, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	row := Db.QueryRowContext(ctx, `SELECT
	Customer_id,
	Customer_name,
	Phone_number,
	Date_created
	FROM Customers
	WHERE Customer_id = ?`, customerid)

	customer := &Customer{}
	err := row.Scan(
		&customer.CustomerID,
		&customer.CustomerName,
		&customer.PhoneNumber,
		&customer.DateCreated,
	)

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		log.Println(err)
		return nil, err
	}
	return customer, nil
}

func removeCustomer(customerID int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err := Db.ExecContext(ctx, `DELETE FROM Customers where Customer_id = ?`, customerID)
	if err != nil {
		log.Println(err.Error())
		return err

	} else {
		log.Printf("DELETE %d", customerID)
	}
	return nil
}

func corsMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token")
		log.Printf("%s %s %s", r.Method, r.URL.Path, r.UserAgent())

		handler.ServeHTTP(w, r)
	})
}

func handleCustomers(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		customerList, err := getCustomerList()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		j, err := json.Marshal(customerList)

		log.Printf(string(j))
		
		if err != nil {
			log.Fatal(err)
		}
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}

	case http.MethodPost:
		var customer Customer
		err := json.NewDecoder(r.Body).Decode(&customer)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		CustomerID, err := insertCustomer(customer)

		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"Customerid:%d"}`, CustomerID)))
	case http.MethodOptions:
		return
	case http.MethodPut:
		var customer Customer
		err := json.NewDecoder(r.Body).Decode(&customer)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		CustomerID, err := putCustomer(customer)

		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"Customerid:%d"}`, CustomerID)))
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func putCustomer(customer Customer) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	result, err := Db.ExecContext(ctx, `UPDATE Customers SET Customer_name = ?, Phone_number = ?, Date_created = ? WHERE Customer_id = ?`,
		customer.CustomerName, customer.PhoneNumber, customer.DateCreated, customer.CustomerID)
	if err != nil {
		log.Println(err.Error())
		return 0, err
	} else {
		log.Printf("Update :", customer)
	}
	updateID, err := result.LastInsertId()
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}
	return int(updateID), nil
}

func insertCustomer(customer Customer) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	result, err := Db.ExecContext(ctx, `INSERT INTO Customers
	(Customer_id,
		Customer_name,
		Phone_number,
		Date_created)
	VALUES (?,?,?,?)`, customer.CustomerID, customer.CustomerName, customer.PhoneNumber, customer.DateCreated)
	if err != nil {
		log.Println(err.Error())
		return 0, err
	} else {
		log.Printf("Insert :", customer)
	}
	insertID, err := result.LastInsertId()
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}
	return int(insertID), nil
}

func main() {
	SetupDB()
	SetupRoutes(basePath)
	log.Fatal(http.ListenAndServe(":5000", nil))
}
