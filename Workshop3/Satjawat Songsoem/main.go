package main

import (
	"bank/handler"
	"bank/logs"
	"bank/repository"
	"bank/service"
	"fmt"
	"log"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

const coursePath = "customer"
const basePath = "/api"

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func SetupDB() *sqlx.DB {
	var err error
	initConfig()
	db, err := sqlx.Open(viper.GetString("db.driver"), fmt.Sprintf("%v:%v@tcp(%v:%v)/%v",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetInt("db.port"),
		viper.GetString("db.database"),
	))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(db)
	db.SetConnMaxIdleTime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db
}

func corsMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token")
		handler.ServeHTTP(w, r)
	})
}

func main() {
	initConfig()
	database := SetupDB()

	router := mux.NewRouter()

	customerRepositoryDB := repository.NewCustomerRepositoryDB(database)
	customerService := service.NewCustomerService(customerRepositoryDB)
	customerHandler := handler.NewCustomerHandler(customerService)

	router.Handle(fmt.Sprintf("%s/%s/{customerID:[0-9]+}", basePath, coursePath),
		corsMiddleware(http.HandlerFunc(customerHandler.GetCustomer))).Methods(http.MethodGet)

	router.Handle(fmt.Sprintf("%s/%s", basePath, coursePath),
		corsMiddleware(http.HandlerFunc(customerHandler.GetCustomers))).Methods(http.MethodGet)

	router.Handle(fmt.Sprintf("%s/%s", basePath, coursePath),
		corsMiddleware(http.HandlerFunc(customerHandler.InsertCustomer))).Methods(http.MethodPost)

	router.Handle(fmt.Sprintf("%s/%s", basePath, coursePath),
		corsMiddleware(http.HandlerFunc(customerHandler.UpdateCustomer))).Methods(http.MethodPut)

	router.Handle(fmt.Sprintf("%s/%s/{customerID:[0-9]+}", basePath, coursePath),
		corsMiddleware(http.HandlerFunc(customerHandler.RemoveCustomer))).Methods(http.MethodDelete)

	logs.Info("Customer service started at port " + viper.GetString("app.port"))
	http.ListenAndServe(fmt.Sprintf(":%v", viper.GetInt("app.port")), router)
}
