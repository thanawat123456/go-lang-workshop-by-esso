package repository

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
)

type customerRepositoryDB struct {
	db *sqlx.DB
}

func NewCustomerRepositoryDB(db *sqlx.DB) customerRepositoryDB {
	return customerRepositoryDB{db: db}
}

func (r customerRepositoryDB) InsertCustomer(customer Customer) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "INSERT INTO customers (Customer_id, Customer_name, Phone_number, Date_created) VALUES (?, ?, ?,?)"
	result, err := r.db.ExecContext(ctx, query, customer.CustomerID, customer.CustomerName, customer.PhoneNumber, customer.DateCreated)
	if err != nil {
		return 0, err
	}
	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return int(lastInsertID), nil
}

func (r customerRepositoryDB) RemoveCustomer(customerID int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "DELETE FROM customers WHERE Customer_id = ?"
	_, err := r.db.ExecContext(ctx, query, customerID)
	if err != nil {
		return err
	}
	return nil
}

func (r customerRepositoryDB) UpdateCustomer(customer Customer) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "UPDATE Customers SET Customer_name = ?, Phone_number = ?, Date_created = ? WHERE Customer_id = ?"
	result, err := r.db.ExecContext(ctx, query, customer.CustomerName, customer.PhoneNumber, customer.DateCreated, customer.CustomerID)
	if err != nil {
		return 0, err
	}
	updateID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return int(updateID), nil
}

func (r customerRepositoryDB) GetAll() ([]Customer, error) {
	customers := []Customer{}
	query := "SELECT Customer_id, Customer_name, Phone_number, Date_created FROM customers"
	err := r.db.Select(&customers, query)

	if err != nil {
		return nil, err
	}

	return customers, nil
}

func (r customerRepositoryDB) GetById(id int) (*Customer, error) {
	customer := Customer{}
	query := "SELECT Customer_id, Customer_name, Phone_number, Date_created FROM customers WHERE Customer_id = ?"
	err := r.db.Get(&customer, query, id)

	if err != nil {
		return nil, err
	}

	return &customer, nil
}
