package main

import (
	"Thanabat/Workshop3/Thanabat/handler"
	"Thanabat/Workshop3/Thanabat/logs"
	"Thanabat/Workshop3/Thanabat/repository"
	"Thanabat/Workshop3/Thanabat/service"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

func main() {
	initTimeZone()
	initConfig()
	db := initDatabase()
	customerRepositoryDB := repository.NewCustomerRepositoryDB(db)
	customerService := service.NewCustomerService(customerRepositoryDB)
	customerHandler := handler.NewCustomerHandler(customerService)

	router := mux.NewRouter()

	// GET endpoints
	router.HandleFunc("/customers", customerHandler.GetCustomers).Methods(http.MethodGet)
	router.HandleFunc("/customers/{CustomerID:[0-9]+}", customerHandler.GetCustomer).Methods(http.MethodGet)
	// POST endpoint
	router.HandleFunc("/customers", customerHandler.InsertCustomer).Methods(http.MethodPost)
	// DELETE endpoint
	router.HandleFunc("/customers/{CustomerID:[0-9]+}", customerHandler.DeleteCustomer).Methods(http.MethodDelete)
	// PUT endpoints
	router.HandleFunc("/customers/{CustomerID:[0-9]+}", customerHandler.UpdateCustomer).Methods(http.MethodPut)

	// log.Printf("Customer service started at port %v\n",viper.GetInt("app.port"))
	logs.Info("Customer service started at port " + viper.GetString("app.port"))
	http.ListenAndServe(fmt.Sprintf(":%v", viper.GetInt("app.port")), router)
}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}
func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict
}
func initDatabase() *sqlx.DB {
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
		viper.GetString("database.host"),
		viper.GetInt("database.port"),
		viper.GetString("database.username"),
		viper.GetString("database.password"),
		viper.GetString("database.dbname"),
		viper.GetString("database.sslmode"),
	)

	db, err := sqlx.Connect(viper.GetString("database.driver"), dsn)
	if err != nil {
		log.Fatal(err)
	}
	db.SetConnMaxLifetime(3 * time.Minute)
	db.SetMaxOpenConns(10)
	db.SetConnMaxIdleTime(10)
	return db
}
