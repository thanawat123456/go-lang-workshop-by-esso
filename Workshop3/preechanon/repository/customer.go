package repository

type Customer struct {
	CustomerId   int    `json:"customerid"`
	CustomerName string `json:"customername"`
	Phone_number string `json:"phone_number"`
	Date_created string `json:"date_created"`
}

type CustomerRepository interface {
	GetAll() ([]Customer, error)
	GetById(int) (*Customer, error)
	UpdateCustomer(Customer, int) (int, error)
	AddCustomer(Customer) (int, error)
	DeleteCustomer(int) error
}
