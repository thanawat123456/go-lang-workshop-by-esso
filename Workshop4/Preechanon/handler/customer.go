package handler

import (
	"encoding/json"
	"fiber_framework/errs"
	"fiber_framework/repository"
	"fiber_framework/service"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

type customerHandler struct {
	customerService service.CustomerService
}

func NewCustomerHandler(customerService service.CustomerService) customerHandler {
	return customerHandler{customerService: customerService}
}

func (h customerHandler) GetCustomers(c *fiber.Ctx) error {
	customers, err := h.customerService.GETCustomers()
	if err != nil {
		return c.Status(http.StatusInternalServerError).SendString(err.Error())
	}
	json_, err := json.Marshal(customers)
	if err != nil {
		return err
	}
	return c.Send(json_)
}

func (h customerHandler) GetCustomer(c *fiber.Ctx) error {
	customerID, _ := strconv.Atoi(c.Params("id"))

	customer, err := h.customerService.GETCustomer(customerID)
	if err != nil {
		return c.Status(http.StatusInternalServerError).SendString(err.Error())
	}

	return c.JSON(customer)
}

func (h customerHandler) AddCustomer(c *fiber.Ctx) error {
	var customer repository.Customer
	if err := c.BodyParser(&customer); err != nil {
		return c.Status(http.StatusInternalServerError).SendString(err.Error())
	}

	customerID, err := h.customerService.ADDCustomer(customer)
	if err != nil {
		return c.Status(http.StatusInternalServerError).SendString(err.Error())
	}
	return c.SendString(fmt.Sprintf("Add successfully with ID %d", customerID))
}

func (h customerHandler) DeleteCustomer(c *fiber.Ctx) error {
	customerID, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(http.StatusBadRequest).SendString(err.Error())
	}

	err = h.customerService.DELETECustomer(customerID)
	if err != nil {
		return c.Status(http.StatusInternalServerError).SendString(err.Error())
	}
	return c.SendString(fmt.Sprintf("Delete successfully with ID %d", customerID))
}

func (h customerHandler) UpdateCustomer(c *fiber.Ctx) error {
	customerID, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(http.StatusBadRequest).SendString(err.Error())
	}

	var customer repository.Customer
	err = c.BodyParser(&customer)
	if err != nil {
		return c.Status(http.StatusInternalServerError).SendString(err.Error())
	}

	customerID_, err := h.customerService.UPDATECustomer(customer, customerID)
	if err != nil {
		appErr, ok := err.(errs.AppError)
		if ok {
			return c.Status(appErr.Code).SendString(appErr.Message)
		}
		return c.Status(http.StatusInternalServerError).SendString(err.Error())
	}

	return c.SendString(fmt.Sprintf("Update successfully with ID %d", customerID_))
}
