package main

import (
	"fmt"
	"strings"
	"time"
	"log"

	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"

	"fiber_framework/handler"
	"fiber_framework/repository"
	"fiber_framework/service"

	"github.com/gofiber/fiber/v2"
	_ "github.com/go-sql-driver/mysql"
)

func apiDb() {

	initConfig()
	initTimeZone()
	db := initDatabase()

	customerRepository := repository.NewCustomerRepositoryDB(db)
	customerService := service.NewCustomerService(customerRepository)
	customerHandler := handler.NewCustomerHandler(customerService)

	app := fiber.New()
	app.Get("/customers",customerHandler.GetCustomers)
	app.Get("/customers/:id",customerHandler.GetCustomer)
	app.Post("/customers",customerHandler.AddCustomer)
	app.Post("/customers/:id",customerHandler.UpdateCustomer)
	app.Delete("/customers/:id",customerHandler.DeleteCustomer)


	err := app.Listen(fmt.Sprintf(":%v", viper.GetInt("app.port")))
	if err != nil {
		log.Fatal(err)
	}

}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {
	apiDb()
}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict
}

func initDatabase() *sqlx.DB {
	db, err := sqlx.Open(viper.GetString("db.driver"), fmt.Sprintf("%v@tcp(%v:%v)/%v",
		viper.GetString("db.username"),
		viper.GetString("db.host"),
		viper.GetString("db.port"),
		viper.GetString("db.database")))
	if err != nil {
		panic(err)
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db
}
