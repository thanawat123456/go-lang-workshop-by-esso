package handler

import (
	"ThanabatSR/Workshop4/ThanabatSR/errs"
	"ThanabatSR/Workshop4/ThanabatSR/service"
	"fmt"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

type customerHandler struct {
	custSrv service.CustomerService
}

func NewCustomerHandler(custSrv service.CustomerService) customerHandler {
	return customerHandler{custSrv: custSrv}
}

func (h customerHandler) GetCustomers(c *fiber.Ctx) error {
	customers, err := h.custSrv.GetCustomers()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}
	return c.JSON(customers)
}

func (h customerHandler) RemoveCustomer(c *fiber.Ctx) error {
	customerID, err := strconv.Atoi(c.Params("customerID"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}
	err = h.custSrv.RemoveCustomer(customerID)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}
	return c.SendString(fmt.Sprintf("Delete Customerid:%v", customerID))
}

func (h customerHandler) InsertCustomer(c *fiber.Ctx) error {
	var customer service.CustomerResponse
	err := c.BodyParser(&customer)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}
	insertedID, err := h.custSrv.InsertCustomer(customer)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}
	return c.SendString(fmt.Sprintf("Insert CustomerId:%v", insertedID))
}


func (h customerHandler) UpdateCustomer(c *fiber.Ctx) error {
    var customer service.CustomerResponse
    err := c.BodyParser(&customer)
    if err != nil {
        return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
    }
    customerID, _ := strconv.Atoi(c.Params("customerID"))
    _, err = h.custSrv.UpdateCustomer(customerID, customer)
    if err != nil {
        return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
    }
    return c.SendString(fmt.Sprintf("Update Customerid:%v", customerID))
}


func (h customerHandler) GetCustomer(c *fiber.Ctx) error {
	customerID, err := strconv.Atoi(c.Params("customerID"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}
	customer, err := h.custSrv.GetCustomer(customerID)
	if err != nil {
		appErr, ok := err.(errs.AppError)
		if ok {
			return c.Status(appErr.Code).SendString(appErr.Message)
		}
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}
	return c.JSON(customer)
}
