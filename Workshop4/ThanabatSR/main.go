package main

import (
	"ThanabatSR/Workshop4/ThanabatSR/handler"
	"ThanabatSR/Workshop4/ThanabatSR/repository"
	"ThanabatSR/Workshop4/ThanabatSR/service"
	"fmt"
	"log"
	"time"
	"github.com/gofiber/fiber/v2/middleware/cors"
	_ "github.com/lib/pq"
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

var db *sqlx.DB

func main() {
	db = SetupDB()
	app := fiber.New()
	app.Use(cors.New())
	app.Use(func(c *fiber.Ctx) error {
		c.Set("Access-Control-Allow-Origin", "*")
		c.Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		c.Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding")
		c.Set("Content-Type", "application/json")
		return c.Next()
	})

	customerRepositoryDB := repository.NewCustomerRepositoryDB(db)
	customerService := service.NewCustomerService(customerRepositoryDB)
	customerHandler := handler.NewCustomerHandler(customerService)

	app.Get("/customer/:customerID", customerHandler.GetCustomer)
	app.Get("/customer", customerHandler.GetCustomers)
	app.Post("/customer", customerHandler.InsertCustomer)
	app.Put("/customer/:customerID", customerHandler.UpdateCustomer)
	app.Delete("/customer/:customerID", customerHandler.RemoveCustomer)

	app.Listen(":8000")
}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func SetupDB() *sqlx.DB {
    var err error
    initConfig()
    db, err = sqlx.Open(viper.GetString("database.driver"), fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
        viper.GetString("database.host"),
        viper.GetInt("database.port"),
        viper.GetString("database.username"),
        viper.GetString("database.password"),
        viper.GetString("database.dbname"),
        viper.GetString("database.sslmode"),
    ))
    if err != nil {
        log.Fatal(err)
    }

    // Set the database connection properties
    db.SetConnMaxIdleTime(time.Minute * 3)
    db.SetMaxOpenConns(10)
    db.SetMaxIdleConns(10)

    return db
}
