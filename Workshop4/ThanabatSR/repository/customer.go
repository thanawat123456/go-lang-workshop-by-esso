package repository

type Customer struct {
	CustomerID   int    `db:"customer_id"`
	CustomerName string `db:"customer_name"`
	PhoneNumber  string `db:"phone_number"`
	DateCreated  string `db:"date_created"`
}

type CustomerRepository interface {
	GetAll() ([]Customer, error)
	GetById(int) (*Customer, error)
	InsertCustomer(Customer) (int, error)
	RemoveCustomer(int) error
	UpdateCustomer(Customer) (int, error)
}
