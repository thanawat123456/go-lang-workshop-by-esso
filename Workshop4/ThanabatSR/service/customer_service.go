package service

import (
	"ThanabatSR/Workshop4/ThanabatSR/errs"
	"ThanabatSR/Workshop4/ThanabatSR/logs"
	"ThanabatSR/Workshop4/ThanabatSR/repository"
	"database/sql"

	"go.uber.org/zap"
)

type customerService struct {
	custRepo repository.CustomerRepository
}

func NewCustomerService(custRepo repository.CustomerRepository) customerService {
	return customerService{custRepo: custRepo}
}

func (s customerService) GetCustomers() ([]CustomerResponse, error) {
	customers, err := s.custRepo.GetAll()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	custResponses := []CustomerResponse{}
	for _, customer := range customers {
		custResponse := CustomerResponse{
			CustomerID:   customer.CustomerID,
			CustomerName: customer.CustomerName,
			PhoneNumber:  customer.PhoneNumber,
			DateCreated:  customer.DateCreated,
		}
		custResponses = append(custResponses, custResponse)
	}
	logs.Info("Get Customers", zap.Any("customers", custResponses))
	return custResponses, nil
}

func (s customerService) RemoveCustomer(id int) error {
	err := s.custRepo.RemoveCustomer(id)
	if err != nil {
		logs.Error(err)
		return errs.NewNotfoundError("Customer Not Found")
	}
	logs.Info("Remove Customer  : ", zap.Int("id", id))
	return nil
}

func (s customerService) InsertCustomer(customerResponse CustomerResponse) (int, error) {
	customer := repository.Customer{
		CustomerName: customerResponse.CustomerName,
		PhoneNumber:  customerResponse.PhoneNumber,
	}

	customerID, err := s.custRepo.InsertCustomer(customer)
	if err != nil {
		logs.Error("Error inserting customer:", zap.Error(err))
		return 0, errs.NewValidationError("Validation Error")
	}
	logs.Info("Insert Customer  : ", zap.Int("id", customerID))
	return customerID, nil
}

func (s customerService) UpdateCustomer(customerID int, customerResponse CustomerResponse) (int, error) {
    customer := repository.Customer{
        CustomerID:   customerID,
        CustomerName: customerResponse.CustomerName,
        PhoneNumber:  customerResponse.PhoneNumber,
    }

    updatedID, err := s.custRepo.UpdateCustomer(customer)
    if err != nil {
        logs.Error(err)
        return 0, errs.NewValidationError("Validation Error")
    }
    logs.Info("Update Customer  : ", zap.Int("id", customerID))
    return updatedID, nil
}

func (s customerService) GetCustomer(id int) (*CustomerResponse, error) {
	customer, err := s.custRepo.GetById(id)
	logs.Info("Get Customer ByID : ", zap.Int("id", id))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotfoundError("Customer Not Found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	custResponse := CustomerResponse{
		CustomerID:   customer.CustomerID,
		CustomerName: customer.CustomerName,
		PhoneNumber:  customer.PhoneNumber,
		DateCreated:  customer.DateCreated,
	}
	logs.Info("Get Customer", zap.Int("id", id), zap.Any("customers", custResponse))
	return &custResponse, nil
}
